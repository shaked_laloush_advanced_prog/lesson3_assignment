#include <Iostream>
#include "sqlite3.h"
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
unordered_map<string, vector<string>> results;

int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurcahse(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
//main project opens the db file and calls the functions
void main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "can't Open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
	}
	carPurcahse(1, 4, db, zErrMsg);
	carPurcahse(12, 4, db, zErrMsg);
	carPurcahse(5, 20, db, zErrMsg);
	balanceTransfer(1, 2, 100, db, zErrMsg);
}
//checks if it's availble to purchase a car
bool carPurcahse(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	results.clear();
	//gets the cars
	string c = "select * from cars where id=" + to_string(carid) + ";";
	const char* C = c.c_str();
	sqlite3_exec(db,C, callback, 0, &zErrMsg);
	auto it = results.begin();
	//gets if availbel
	string available= it->second.at(0).c_str();
	it++;
	it++;
	it++;
	//gets the cost
	string cost = it->second.at(0).c_str();
	//turns how much it costs into an int
	int costi = stoi(cost);
	results.clear();
	//gets account info
	string s = "select * from accounts where Buyer_id=" + to_string(buyerid) + ";";
	const char* S = s.c_str();
	sqlite3_exec(db, S, callback, 0, &zErrMsg);
	it = results.begin();
	it++;
	it++;
	//gets how much money the account has and turns it into int
	string money = it->second.at(0).c_str();
	int moneyi = stoi(money);
	//checks if he has enough money and if it's available
	if (moneyi >= costi && available.compare("1")==0)
	{
		moneyi -= costi;
		//updates the new info
		s = "update accounts set balance=" + to_string(moneyi) + " where Buyer_id=" + to_string(buyerid) + ";";
		S = s.c_str();
		sqlite3_exec(db, S, callback, 0, &zErrMsg);
		s = "update cars set available=0 where id=" + to_string(carid) + ";";
		S = s.c_str();
		sqlite3_exec(db, S, callback, 0, &zErrMsg);
		//returns true and it means everything was fine
		return(true);
	}
	//returns false and it means something was wrong
	return(false);
}
//transfers money from one account to the other
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	results.clear();
	//gets the balance from from
	string c = "select balance from accounts where id=" + to_string(from) + ";";
	const char* C = c.c_str();
	sqlite3_exec(db, C, callback, 0, &zErrMsg);
	
	auto it = results.begin();
	//gets the money and turns it into an int
	string money = it->second.at(0).c_str();
	int moneyi = stoi(money);
	results.clear();
	//gets the balance of to
	c = "select balance from accounts where id=" + to_string(to) + ";";
	C = c.c_str();
	sqlite3_exec(db, C, callback, 0, &zErrMsg);
	it = results.begin();
	//gets his current money
	string currmoneyto = it->second.at(0).c_str();
	int currmoneytoi = stoi(money);
	//checks if from has enough money to transfer
	if (moneyi >= amount)
	{	
		//updates money in both accounts
		sqlite3_exec(db, "begin transaction;", callback, 0, &zErrMsg);
		c = "update accounts set balance="+to_string(moneyi-amount) +" where id=" + to_string(from) + ";";
		C = c.c_str();
		sqlite3_exec(db, C, callback, 0, &zErrMsg);
		
		c = "update accounts set balance=" + to_string(currmoneytoi+amount) + " where id=" + to_string(to) + ";";
		C = c.c_str();
		sqlite3_exec(db, C, callback, 0, &zErrMsg);
		sqlite3_exec(db, "end transaction;", callback, 0, &zErrMsg);
		//returns true if the transfer was successful
		return(true);
	}
	//returns false if the transfer did't make it
	return(false);
	


}
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}
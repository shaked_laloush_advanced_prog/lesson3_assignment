#include <Iostream>
#include "sqlite3.h"
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol);
void main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	//opens database
	rc = sqlite3_open("FirstPart.db", &db);
	if (rc)
	{
		cout << "can't Open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
	}
	//creats new table d people and two collums id and name
	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement,name string)", callback, 0, &zErrMsg);
	//inserts 3 names without id because it changes intself
	rc = sqlite3_exec(db, "insert into people(name) values(Eric)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values(Ed)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values(Merla)", callback, 0, &zErrMsg);
	//updates one of the names to another name 
	rc = sqlite3_exec(db, "update people set name=Nami where name=Merla", callback, 0, &zErrMsg);

}
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}